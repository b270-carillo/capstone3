import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function ProductCard({productProp}) {

    console.log(productProp);

    const {_id, name, description, price, src } = productProp;

    return (
        <Card style={{ width: '30rem' }} className="p-1 m-2 d-flex d-inline-flex" >
            <Card.Img variant="top" src={src} style={{ height: '350px' }}/>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {price}</Card.Text>
                <Button variant="primary" as={Link} to={`/products/${_id}`}>Buy Now</Button>
            </Card.Body>
        </Card>
    )
}