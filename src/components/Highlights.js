import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights() {
	return (
		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				    <Card.Body>
				        <Card.Title>Buy One, Get Two</Card.Title>		        
				        <Card.Text>
				          Aenean pharetra magna ac placerat vestibulum lectus. Nisl tincidunt eget nullam non nisi est sit amet. Fames ac turpis egestas sed tempus urna et. Ac placerat vestibulum lectus mauris ultrices. Eu mi bibendum neque egestas. Egestas dui id ornare arcu odio ut sem nulla pharetra. Eget arcu dictum varius duis at consectetur lorem donec massa. Nisl vel pretium lectus quam. Molestie a iaculis at erat pellentesque. Morbi tincidunt ornare massa eget egestas purus viverra accumsan in. Gravida in fermentum et sollicitudin. Faucibus in ornare quam viverra orci. Dui sapien eget mi proin. Habitasse platea dictumst vestibulum rhoncus est pellentesque elit ullamcorper. Sollicitudin ac orci phasellus egestas tellus rutrum tellus pellentesque.
				        </Card.Text>				       
				    </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				    <Card.Body>
				        <Card.Title>5% Off Hurry Up</Card.Title>		        
				        <Card.Text>
				          Adipiscing bibendum est ultricies integer. Lorem dolor sed viverra ipsum nunc aliquet bibendum enim. Sagittis orci a scelerisque purus semper eget duis at tellus. Ut pharetra sit amet aliquam id diam maecenas ultricies mi. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus a. Ut consequat semper viverra nam libero justo laoreet sit. Donec massa sapien faucibus et. Amet nulla facilisi morbi tempus iaculis urna id volutpat lacus. In metus vulputate eu scelerisque. Quam vulputate dignissim suspendisse in est ante in. Odio ut sem nulla pharetra diam. Semper auctor neque vitae tempus quam. Condimentum mattis pellentesque id nibh tortor id aliquet lectus. Eu mi bibendum neque egestas congue quisque egestas diam.
				        </Card.Text>				       
				    </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				    <Card.Body>
				        <Card.Title>Free Shipping All Over The World</Card.Title>		        
				        <Card.Text>
				          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quam adipiscing vitae proin sagittis. Tincidunt augue interdum velit euismod in pellentesque massa placerat duis. Nisl tincidunt eget nullam non nisi est. Ut morbi tincidunt augue interdum velit euismod in pellentesque massa. Ut placerat orci nulla pellentesque dignissim enim. Placerat duis ultricies lacus sed turpis tincidunt id aliquet risus. Tellus rutrum tellus pellentesque eu tincidunt. In fermentum et sollicitudin ac orci phasellus. Tincidunt augue interdum velit euismod in pellentesque massa placerat. Neque sodales ut etiam sit amet nisl purus in. Ut enim blandit volutpat maecenas volutpat blandit aliquam. Proin sagittis nisl rhoncus mattis rhoncus. Amet nulla facilisi morbi tempus iaculis urna id volutpat. 
				        </Card.Text>				       
				    </Card.Body>
				</Card>
			</Col>
		</Row>
	)
}