import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function ProductView() {

	const navigate = useNavigate();

	// To be able to obtain user ID so we can buy a product
	const { user } = useContext(UserContext);

	// The "useParams" hook allows us to retrieve the productId passed via the URL
	const { productId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const checkout = (productId) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
			method: "POST",
			headers: {
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res=> res.json())
		.then(data => {
			console.log(data);

			if(data) {
				Swal.fire({
					title: "Successfully purchased!",
					icon: "success",
					text: "You have successfully purchased this product."
				})

				navigate("/products/");

			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	useEffect(() => {

		console.log(productId)
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})

	}, [productId])

	return(
		<Container>
			<Row>
				<Col>
					<Card>
					    <Card.Body className="text-center">
					        <Card.Title>{name}</Card.Title>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>PhP {price}</Card.Text>
					        {user.id !== null
					        	?
					        	<Button variant="primary" onClick={() => checkout(productId)}>Buy Now</Button>
					        	:
					        	<Button variant="danger" as={Link} to="/login">Log in to Buy</Button>
					        }
					    </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}
