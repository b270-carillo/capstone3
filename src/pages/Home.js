import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home() {

	const data = {
	    title: "CapStore",
	    content: "Buy One Take two HURRY UP! ",
	    destination: "/products",
	    label: "Buy now!"
	}
	
	return (
		<Fragment>
			<Banner data={data}/>
			<Highlights />
		</Fragment>
	)
}
